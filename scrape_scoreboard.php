<?php
include_once('../_libraries/simplehtmldom/simple_html_dom.php');

function scraping_sox_scores() {
    // get yesterday's date

    // create HTML DOM
    $html = file_get_html('http://scores.espn.go.com/mlb/scoreboard');
    foreach($html->find('div.mod-mlb-scorebox') as $scorediv) {
        $item['inning'] = trim($scorediv->find('div.game-status', 0)->plaintext);
    // get scoreboard table
        foreach($scorediv->find('table.game-details') as $boxscore) {
                //get home team
                $item['home'] = trim($boxscore->find('tr td.team', 1)->plaintext);
                // get home scores by inning
                $item['hs1'] = trim($boxscore->find('td[id$=hls0]', 0)->plaintext);
                $item['hs2'] = trim($boxscore->find('td[id$=hls1]', 0)->plaintext);
                $item['hs3'] = trim($boxscore->find('td[id$=hls2]', 0)->plaintext);
                $item['hs4'] = trim($boxscore->find('td[id$=hls3]', 0)->plaintext);
                $item['hs5'] = trim($boxscore->find('td[id$=hls4]', 0)->plaintext);
                $item['hs6'] = trim($boxscore->find('td[id$=hls5]', 0)->plaintext);
                $item['hs7'] = trim($boxscore->find('td[id$=hls6]', 0)->plaintext);
                $item['hs8'] = trim($boxscore->find('td[id$=hls7]', 0)->plaintext);
                $item['hs9'] = trim($boxscore->find('td[id$=hls8]', 0)->plaintext);
                // get home runs
                $item['hruns'] = trim($boxscore->find('td[id$=hlsT]', 0)->plaintext);
                // get home hits
                $item['hhits'] = trim($boxscore->find('td[id$=hlsH]', 0)->plaintext);
                // get home errors
                $item['herrors'] = trim($boxscore->find('td[id$=hlsE]', 0)->plaintext);
                // get away team
                $item['away'] = trim($boxscore->find('tr td.team', 0)->plaintext);
                // get away scores by inning
                $item['as1'] = trim($boxscore->find('td[id$=als0]', 0)->plaintext);
                $item['as2'] = trim($boxscore->find('td[id$=als1]', 0)->plaintext);
                $item['as3'] = trim($boxscore->find('td[id$=als2]', 0)->plaintext);
                $item['as4'] = trim($boxscore->find('td[id$=als3]', 0)->plaintext);
                $item['as5'] = trim($boxscore->find('td[id$=als4]', 0)->plaintext);
                $item['as6'] = trim($boxscore->find('td[id$=als5]', 0)->plaintext);
                $item['as7'] = trim($boxscore->find('td[id$=als6]', 0)->plaintext);
                $item['as8'] = trim($boxscore->find('td[id$=als7]', 0)->plaintext);
                $item['as9'] = trim($boxscore->find('td[id$=als8]', 0)->plaintext);
                // get away runs
                $item['aruns'] = trim($boxscore->find('td[id$=alsT]', 0)->plaintext);
                // get away hits
                $item['ahits'] = trim($boxscore->find('td[id$=alsH]', 0)->plaintext);
                // get away errors
                $item['aerrors'] = trim($boxscore->find('td[id$=alsE]', 0)->plaintext);
            $ret[] = $item;
        }
    }
    // clean up memory
    $html->clear();
    unset($html);

    return $ret;
}

// -----------------------------------------------------------------------------
// test it!
$ret = scraping_sox_scores();
?>
<?php
$c = 0;
$i = 0;
$otherScores = array();
foreach($ret as $v) {
    if ((($v['home'] == 'BOS')||($v['away'] == 'BOS')) && ($c != 1)){
        if($v['home'] == 'BOS'){$v['home'] = 'BOSTON';}
        echo '<div id="boxscore"><h2>FENWAY PARK</h2><table>';
        echo '<tr>';
        echo '<td></td><td>1</td><td>2</td><td>3</td><td>4</td><td>5</td><td>6</td><td>7</td><td>8</td><td>9</td><td class="runs">R</td><td>H</td><td>E</td>';
        echo '</tr>';
        echo '<td id="away"><center>'.$v['away'].'</center></td><td class="inset"><center>';
            echo $v['as1']. '</center></td><td class="inset"><center>';
            echo $v['as2']. '</center></td><td class="inset"><center>';
            echo $v['as3']. '</center></td><td class="inset"><center>';
            echo $v['as4']. '</center></td><td class="inset"><center>';
            echo $v['as5']. '</center></td><td class="inset"><center>';
            echo $v['as6']. '</center></td><td class="inset"><center>';
            echo $v['as7']. '</center></td><td class="inset"><center>';
            echo $v['as8']. '</center></td><td class="inset"><center>';
            echo $v['as9']. '</center></td><td class="inset runs"><center>';
            echo $v['aruns']. '</center></td><td class="inset"><center>';
            echo $v['ahits']. '</center></td><td class="inset"><center>';
            echo $v['aerrors']. '</center></td>';
            echo '</tr><tr>';
        echo '<td id="home"><center>'.$v['home'].'</center></td><td class="inset"><center>';
            echo $v['hs1']. '</center></td><td class="inset"><center>';
            echo $v['hs2']. '</center></td><td class="inset"><center>';
            echo $v['hs3']. '</center></td><td class="inset"><center>';
            echo $v['hs4']. '</center></td><td class="inset"><center>';
            echo $v['hs5']. '</center></td><td class="inset"><center>';
            echo $v['hs6']. '</center></td><td class="inset"><center>';
            echo $v['hs7']. '</center></td><td class="inset"><center>';
            echo $v['hs8']. '</center></td><td class="inset"><center>';
            echo $v['hs9']. '</center></td><td class="inset runs"><center>';
            echo $v['hruns']. '</center></td><td class="inset"><center>';
            echo $v['hhits']. '</center></td><td class="inset"><center>';
            echo $v['herrors']. '</center></td>';
            echo '</tr>';
        echo '</table>';
        $c = 1;
    }else{
        $scoreTable = '<table class="other-score"><tr><td>'.$v['away'].'</td><td class="inset oscore"><center>'.$v['aruns'].'</center></td></tr>';
        $scoreTable .= '<tr><td>'.$v['home'].'</td><td class="inset oscore"><center>'.$v['hruns'].'</center></td></tr></table>';
        if(substr($v['inning'], -5, 2) != 'PM'){
            if(($v['inning'] != 'Delayed') && ($v['inning'] != 'Final')){
                $scoreTable .= '<span class="inset oscore oinning"><center>'.substr($v['inning'], 4, 1).'</center></span>';
            }else{
                if($v['inning'] == 'Delayed'){
                    $scoreTable .= '<span class="inset oscore oinning"><center>D</center></span>';
                }
                if($v['inning'] == 'Final'){
                    $scoreTable .= '<span class="inset oscore oinning"><center>F</center></span>';
                }
            }
        }else{
            $scoreTable .= '<span class="inset oscore oinning">&nbsp;</span>';
        }
        $otherScores[$i] = $scoreTable;
        $i++;
    }
}
// if there is no game on the schedule, create an empty scoreboard so things 
// don't look broken
if ($c != 1){
    ?>
<div id="boxscore"><h2>FENWAY PARK</h2>
    <table>
        <tr><td></td><td>1</td><td>2</td><td>3</td><td>4</td><td>5</td><td>6</td><td>7</td><td>8</td><td>9</td><td class="runs">R</td><td>H</td><td>E</td></tr>
        <tr>
            <td id="away"></td>
            <?php
            for ($i=0; $i < 9; $i++) { 
                print '<td class="inset">&nbsp;</td>';
            }
            ?>
            <td class="inset runs">&nbsp;</td>
            <td class="inset">&nbsp;</td>
            <td class="inset">&nbsp;</td>
        </tr>
        <tr>
            <td id="home">BOSTON</td>
            <?php
            for ($i=0; $i < 9; $i++) { 
                print '<td class="inset">&nbsp;</td>';
            }
            ?>
            <td class="inset runs">&nbsp;</td>
            <td class="inset">&nbsp;</td>
            <td class="inset">&nbsp;</td>
        </tr>
    </table>
    <?php
}
?>