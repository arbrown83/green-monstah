<?php
include_once('../_libraries/simplehtmldom/simple_html_dom.php');

function scraping_sox_standings() {
    $year = date('Y');
    // create HTML DOM
    $html = file_get_html('http://en.wikipedia.org/wiki/Template:'.$year.'_AL_East_standings');

    // get article block
    foreach($html->find('table.wikitable tr') as $standings) {

            $item['team'] = trim($standings->find('td', 0)->plaintext);
            $item['wins'] = trim($standings->find('td', 1)->plaintext);
            $item['losses'] = trim($standings->find('td', 2)->plaintext);
            $item['gb'] = trim($standings->find('td', 4)->plaintext);

        $ret[] = $item;
    }
    
    // clean up memory
    $html->clear();
    unset($html);

    return $ret;
}

// -----------------------------------------------------------------------------
// test it!
$ret = scraping_sox_standings();
?>
<h2><center>AL EAST</center></h2>
<table>
<thead>
    <tr>
        <th></th>
        <th>W</th>
        <th>L</th>
        <th>GB</th>
<?php
foreach($ret as $v) {
    switch ($v['team']) {
        case 'New York Yankees':
            $v['team'] = 'New York';
            break;
        case 'Baltimore Orioles':
            $v['team'] = 'Baltimore';
            break;
        case 'Boston Red Sox':
            $v['team'] = 'Boston';
            break;
        case 'Toronto Blue Jays':
            $v['team'] = 'Toronto';
            break;
        case 'Tampa Bay Rays':
            $v['team'] = 'Tampa Bay';
            break;
    }
    print '<tr>';
    print '<td>'.strtoupper($v['team']).'</td>';
    print '<td>'.$v['wins'].'</td>';
    print '<td>'.$v['losses'].'</td>';
    print '<td>'.$v['gb'].'</td>';
    print '</tr>';
}
print '</table>';
?>